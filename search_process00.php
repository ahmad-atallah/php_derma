
<?php
//$page ="search";
include("header.php");
?>

<section id="main" class="wrapper">
  <div class="container">
    <header class="major special">
      <!--<h2>Hello, <?php echo $_SESSION['f_name'].' '.$_SESSION['l_name']; ?></h2>-->
      <p>Now, please select your search term and specify what is it exactly</p>
    </header>

    <section>
      <form method="post" action="search_process.php">

        <div class="row uniform 50%">
          <div class="12u$">
            <input type="text" name="search_term" id="search" value="" placeholder="Specify here what your search term is exactly or just leave it empty" />
          </div>

          <!-- Select the type of the search term -->

          <div class="12u$">
            <div class="select-wrapper">
              <select name="search_type" id="category">
                <option value="">Select the type of the above search term or just leave it empty</option>
                <option value="Doctor_ID">Doctor ID</option>
                <option value="First_Name">First Name</option>
                <option value="Mid_Name">Middle Name</option>
                <option value="Last_Name">Last Name</option>
                <option value="Doc_Contacts">Doctor Contacts</option>
                <option value="Title">Title</option>
                <option value="Age">Age</option>
                <option value="Sex">Sex</option>
                <option value="Clinic_no">Clinic No.</option>
                <option value="Access">Access Level</option>
              </select>
            </div>
          </div>

          <!-- Select the Attribute to be displayed -->
          <header class="major special" style="margin: 20px auto; padding-left: 20%;">
            <p>Now, Select the attirbutes you'd like to display</p>
          </header>

          <div class="6u 12u$(small)">
            <input type="checkbox" id="ID_CB" name="Doctor_ID">
            <label for="ID_CB">Doctor ID</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="FN_CB" name="First_Name">
            <label for="FN_CB">First Name</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="MN_CB" name="Mid_Name">
            <label for="MN_CB">Middle Name</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="LN_CB" name="Last_Name">
            <label for="LN_CB">Last Name</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="T_CB" name="Title">
            <label for="T_CB">Title</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="Age_CB" name="Age">
            <label for="Age_CB">Age</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="Sex_CB" name="Sex">
            <label for="Sex_CB">Sex</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="CN_CB" name="Clinic_no">
            <label for="CN_CB">Clinic No.</label>
          </div>
          <div class="6u 12u$(small)">
            <input type="checkbox" id="MN_CB" name="Mid_Name">
            <label for="MN_CB">Middle Name</label>
          </div>

          <div class="12u$" style="margin: auto !important;">
            <ul class="actions" style="width: 10%; margin: 50px auto 0;">
              <li><input type="submit" name="submit" value="search" class="special" /></li>
            </ul>
          </div>

        </div>
      </form>
    </section>

  </div>
</section>
