<?php
$page ="insert";
include("header.php");
?>

<section id="main" class="wrapper">
  <div class="container">
    <header class="major special">
      <!-- <h2>Search below</h2> -->
      <p style="color: #E34B4B"><?php if (isset($_GET["fail"])) echo 'The entry was not added successfully' ?></p>
      <p style="color: #E34B4B"><?php if (isset($_GET["fail1"])) echo 'Please, make sure you enter the data correctly' ?></p>  <!--derma 1.1 -->


      <p>Insert the data you want to add to your new entry from below</p>
    </header>

    <section>
      <form method="post" action="insert_process.php">
        <div class="row uniform 50%">

          <div class="6u 12u$(xsmall)">
            <input type="text" name="DoctorIDAdd" id="name" value="" placeholder="Doctor ID" />
          </div>
          <div class="6u$ 12u$(xsmall)">
            <input type="text" name="FirstNameAdd" id="name" value="" placeholder="First Name" />
          </div>
          <div class="6u 12u$(xsmall)">
            <input type="text" name="MiddleNameAdd" id="name" value="" placeholder="Middle Name" />
          </div>
          <div class="6u$ 12u$(xsmall)">
            <input type="text" name="LastNameAdd" id="name" value="" placeholder="Last Name" />
          </div>
          <div class="6u 12u$(xsmall)">
            <input type="text" name="TitleAdd" id="name" value="" placeholder="Title" />
          </div>
          <div class="6u$ 12u$(xsmall)">
            <input type="text" name="AgeAdd" id="name" value="" placeholder="Age" />
          </div>
          <div class="6u 12u$(xsmall)">
            <input type="text" name="SexAdd" id="name" value="" placeholder="Sex" />
          </div>
          <div class="6u 12u$(xsmall)">
            <input type="text" name="AccessLevelAdd" id="name" value="" placeholder="Access Level" />
          </div>

          <div class="12u$">
            <ul class="actions" style="width: 8%; margin: 50px auto 0;">
              <li><input type="submit" name="submit" value="ADD" class="special" /></li>
            </ul>
          </div>

        </div>
      </form>
    </section>
  </div>
</section>

