<?php
include("header.php");
?>

<?php

include("connection.php");

//Loading all search quese in variables
session_start();
$table = $_SESSION['table'];

$att = '';
$val = '';

if ($_POST['DoctorIDAdd'] != '') {
  $ID = $_POST['DoctorIDAdd'];
  $val = $val.$ID.', ';
  $att = $att.'Doctor_ID'.', ';
}
if ($_POST['FirstNameAdd'] != '') {
  $FN = $_POST['FirstNameAdd'];
  $FN = "'".$FN."'";
  $val = $val.$FN.', ';
  $att = $att.'First_Name'.', ';
}
if ($_POST['MiddleNameAdd'] != '') {
  $MN = $_POST['MiddleNameAdd'];
  $MN = "'".$MN."'";
  $val = $val.$MN.', ';
  $att = $att.'Mid_Name'.', ';
}
if ($_POST['LastNameAdd'] != '') {
  $LN = $_POST['LastNameAdd'];
  $LN = "'".$LN."'";
  $val = $val.$LN.', ';
  $att = $att.'Last_Name'.', ';
}
if ($_POST['TitleAdd'] != '') {
  $title = $_POST['TitleAdd'];
  $title = "'".$title."'";
  $val = $val.$title.', ';
  $att = $att.'Title'.', ';
}
if ($_POST['AgeAdd'] != '') {
  $age = $_POST['AgeAdd'];
  $val = $val.$age.', ';
  $att = $att.'Age'.', ';
}
if ($_POST['SexAdd'] != '') {
  $sex = $_POST['SexAdd'];
  $sex = "'".$sex."'";
  $val = $val.$sex.', ';
  $att = $att.'Sex'.', ';
}
if ($_POST['AccessLevelAdd'] != '') {
  $access = $_POST['AccessLevelAdd'];
  $val = $val.$access.', ';
  $att = $att.'Access'.', ';
}


$att = substr($att, 0, -2);
$val = substr($val, 0, -2);

//Running Query
try {

  $results = "INSERT INTO $table ($att) VALUES ($val)";
	$db->query($results);

} 
catch (EXCEPTION $e) {                  /*derma 1.1*/
 if ($val == '') {      
   header('Location: insert_process00.php?fail1');
 }
 else { header('Location: insert_process00.php?fail');

 } }
?>

<!-- Loading HTML -->

<section id="main" class="wrapper">
  <div class="container">
    <header class="major special">
      <h2>Entry Added succesfully</h2>
    </header>

    <section>
      <form method="post" action="search_process00.php">

        <div class="row" style="padding: 0 0 50px 35%;">
          <div class="6u 12u$(xsmall)" style="float: none;">
            <ul class="actions fit">
              <li><a href="insert_process00.php" class="button special">Add Again</a></li>
              <li><a href="datacenter.php" class="button alt fit">Back</a></li>
            </ul>
          </div>
        </div>

      </form>
    </section>

  </div>
</section>
