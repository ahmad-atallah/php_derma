
<!DOCTYPE HTML>
<html>
	<head>
		<title>Happy Derma</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header" class="alt">
				<h1><strong><a href="index.php" class="<?php if ($page != "home") echo "diff-color"?>">Happy Derma</a></strong></h1>
				<nav id="nav">
					<ul>
						<li><a href="index.php" class="<?php if ($page != "home") echo "diff-color" ?>">Home</a></li>
					    <li><a href="request_process00.php" class="<?php if ($page != "home") echo "diff-color" ?>">Request</a></li>
						<li><a href="verify.php" class="<?php if ($page != "home") echo "diff-color" ?>">Data Centre</a></li>

					</ul>
				</nav>
			</header>
