<?php
$page ="data center";
include("header.php");
session_start();
?>
<section id="mainplus" class="wrapper">    <!--derma 1.1-->
  <div class="container">
    <header class="major special">
      <h2>Hello, Dr. <?php echo $_SESSION['f_name'].' '.$_SESSION['l_name']; ?></h2>
      <!--<p>Please, Select the table you want to search, add to or remove entries from</p>-->
    <section>
        <div class="box alt">
          <div class="row">
              <div class="4u 12u$(xsmall)">
                <span class="image fit plus">
                  <img src="images/outline-2autotranscribe_1.svg" alt="" />
                        <a href="search.php" class="button special fit plus">Search</a>
                </span>
              </div>
              <div class="4u 12u$(xsmall)">
                <span class="image fit plus">
                  <img src="images/outline-5edit.svg" alt="" />
                  <a href="insert.php" class="button special fit plus ">Add</a>
                </span>
              </div>
              <div class="4u 12u$(xsmall)">
               <span class="image  fit plus">
                  <img src="images/outline-6export.svg" alt="" />
                  <a href="delete.php" class="button special fit plus">Delete</a>
                </span>
              </div> 
            </div>
          </div>
          
        </div>
 <hr>

        <section>
        <ul class="actions" id="another">
           <li><a href="request_process0.php" class="button alt fit">Online Requests List</a></li>
         </ul>
    </section>
    </section>
    </header>
  </div>
</section>
