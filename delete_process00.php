

<?php
$page ="delete";
include("header.php");
?>

<section id="main" class="wrapper">
  <div class="container">
    <header class="major special">
      <!-- <h2>Search below</h2> -->
      <p style="color: #E34B4B"><?php  if (isset($_GET["fail"])) echo 'The entry was not deleted successfully , ID you entered is not a number  ' ?></p>
       <p style="color: #E34B4B"><?php if (isset($_GET["fail1"])) echo 'ID not Exist.' ?></p>
      <p>Insert the ID of the doctor you wish to delete</p>
    </header>

    <section>
      <form method="post" action="delete_process.php">
        <div class="row uniform 50%">

          <div class="6u 12u$(xsmall)">
            <input type="text" name="DoctorIDDelete" id="name" value="" placeholder="Doctor ID" />
          </div>

          <div class="12u$">
            <ul class="actions" style="width: 8%; margin: 50px auto 0;">
              <li><input type="submit" name="submit" value="Delete" class="special" /></li>
            </ul>
          </div>

        </div>
      </form>
    </section>

  </div>
</section>
