<?php

//Connecting to Database
include("connection.php");


//Loading all search quese in variables
session_start();
$table = $_SESSION['table'];
if ($_POST['search_term'] != '' && $_POST['search_type'] != ''){
$search_term = $_POST['search_term'];
$search_type = $_POST['search_type'];
	if(is_string($search_term)) { //To make the string suitable for the SQL Syntax
		$search_term = "'".$search_term."'";
	}
}
$J=0;
if (isset($_POST['Doctor_ID'])) {
    $Display[$J++] = 'Doctor_ID';
}
if (isset($_POST['First_Name'])) {
    $Display[$J++] = 'First_Name';
}
if (isset($_POST['Mid_Name'])) {
    $Display[$J++] = 'Mid_Name';
}
if (isset($_POST['Last_Name'])) {
    $Display[$J++] = 'Last_Name';
}
if (isset($_POST['Age'])) {
    $Display[$J++] = 'Age';
}
if (isset($_POST['Sex'])) {
    $Display[$J++] = 'Sex';
}
if (isset($_POST['Title'])) {
    $Display[$J++] = 'Title';
}
if (isset($_POST['Clinic_no'])) {
    $Display[$J++] = 'Clinic_no';
}
if (isset($_POST['Access'])) {
    $Display[$J++] = 'Access';
}

$J = 0;
$att='';
while(isset($Display[$J])) {
	$att = $att.$Display[$J].', ';
	$J++;
}
$att = substr($att, 0, -2);

if ($att=='') {
	    $Display[$J++] = 'Doctor_ID';
	    $Display[$J++] = 'First_Name';
	    $Display[$J++] = 'Mid_Name';
	    $Display[$J++] = 'Last_Name';
	    $Display[$J++] = 'Age';
	    $Display[$J++] = 'Sex';
	    $Display[$J++] = 'Title';
	    $Display[$J++] = 'Clinic_no';
	    $Display[$J++] = 'Access';
}

//Running Query
try {

if($_POST['search_term'] == '' && $_POST['search_type'] != '')
     header('Location: search.php?fail');
else if($_POST['search_term'] != '' && $_POST['search_type'] == '')
     header('Location: search.php?fail1');

else if ($_POST['search_term'] != '' && $_POST['search_type'] != '') {

	if ($att == '') {
		$results = $db->query("SELECT*FROM $table WHERE $search_term=$search_type");
		$show = $results->fetchAll(PDO::FETCH_ASSOC);
	}

	else {

		$results = $db->query("SELECT $att FROM $table WHERE $search_term=$search_type");
		$show = $results->fetchAll(PDO::FETCH_ASSOC);
	}
}

else {

	if ($att == '') {
		$results = $db->query("SELECT*FROM $table");
		$show = $results->fetchAll(PDO::FETCH_ASSOC);
	}
	else {
		$results = $db->query("SELECT $att FROM $table");
		$show = $results->fetchAll(PDO::FETCH_ASSOC);
	}
}

} catch (EXCEPTION $e) {

	echo "Failure importing data!";
	exit;
}

//Select the required search term and attribute



?>
<!-- Loading HTML -->
<?php
$page ="search";
include("header.php");
?>

<section id="main" class="wrapper">
  <div class="container">
    <header class="major special">
      <h2>Search Results</h2>
    </header>
  </div>
</section>

<!-- Viewing Data -->
<div class="table-wrapper">
  <table class="alt">
    <thead>
      <tr>
				<?php for($k=0; $k<$J; $k++) { ?> <th><?php echo $Display[$k] ?></th> <?php } ?>
        <!-- <th>First NAME</th>
        <th>Last NAME</th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th> -->
      </tr>
    </thead>
    <tbody>
				<?php
					$n=0;
					while ( isset($show[$n]) ) {
						?><tr><?php
						for($l=0; $l<$J; $l++) {
							$m = $Display[$l]
							?>
								<td><?php echo $show[$n][$m]; ?></td>
							<?php
						}
						$n++;
						?></tr><?php
			    }
				?>
        <!-- <td><?php //echo $show[0]["First_Name"]; ?></td>
        <td><?php //echo $show[0]["Last_Name"]; ?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td> -->
    </tbody>
  </table>
</div>

<ul class="actions" id="another">
  <li><a href="search_process00.php" class="button special">Make another search</a></li>
    <li><a href="datacenter.php" class="button alt fit">Back</a></li>
</ul>
