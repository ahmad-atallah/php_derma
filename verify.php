<?php
$page ="search";
include("header.php");
?>

<section id="main" class="wrapper">
  <div class="container">
    <header class="major special">
      <!-- <h2>Search below</h2> -->
      <p style="color: #E34B4B"><?php if (isset($_GET["fail"])) echo 'Access denied!' ?></p>
	  <p style="color: #E34B4B"><?php if (isset($_GET["fail*"])) echo 'NOT CONNECTED!' ?></p>

      <p>You have to be a verified member of the clinic to go further</p>
    </header>

    <section>
      <form method="post" action="verify_process.php">
        <div class="row uniform 50%">

          <div class="6u 12u$(xsmall)">
            <input type="text" name="lastn" id="name" value="" placeholder="Last Name" />
          </div>
          <div class="6u$ 12u$(xsmall)">
            <input type="text" name="password" id="email" value="" placeholder="Your personal ID" />
          </div>

          <div class="12u$">
            <ul class="actions" style="width: 10%; margin: 50px auto 0;">
              <li><input type="submit" name="submit" value="Login" class="special" /></li>

            </ul>
          </div>

        </div>
      </form>
    </section>

  </div>
</section>
